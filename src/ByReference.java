public class ByReference {
        //changes will be in the local variable only.	 }
        public static void main(String args[]) {
            ByReference reference = new ByReference();
            System.out.println("before change number = " + reference.num);
            reference.changeNum(reference);
            System.out.println("after change number = " + reference.num);
        }
        //we are declare first then we try to change this value using change() method then check in main method
        int num = 70;
        void changeNum(ByReference reference) {
            reference.num += 40;
        }
}
