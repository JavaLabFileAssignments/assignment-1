public class ByValue {
    //changes will be in the local variable only
    public static void main(String args[]) {
        ByValue value = new ByValue();
        System.out.println("before change number = " + value.num);
        value.changeNum(120);
        System.out.println("after change number = " + value.num);
    }
    //we are declare first then we try to change this value using change() method then check in main method
    int num = 70;
    void changeNum(int data) {
        data += 40;
    }
}
